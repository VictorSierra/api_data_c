# FROM node:carbon
# WORKDIR /usr/src/app 
# RUN mkdir api_data

# WORKDIR /usr/src/app/api_data
# RUN mkdir uploads

# COPY . .

# RUN npm install
# CMD [ "npm", "start" ]

FROM node:carbon
RUN apt-get install git-core 
WORKDIR /usr/src/app
RUN git clone https://VictorSierra@bitbucket.org/VictorSierra/api_data_c.git
WORKDIR /usr/src/app/api_data_c
RUN mkdir uploads
RUN npm install
CMD [ "npm", "start" ]