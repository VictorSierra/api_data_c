'use strict'
let express = require('express');
let api = express.Router();

let PublicacionController = require('../controllers/publicacion.controller');
let files = require('../controllers/actas.controller');

api.get('/descarga', PublicacionController.consultajson);

api.get('/GOB_DET_ENTIDAD', PublicacionController.consultajsonGubernatura_det_entidad);
api.get('/GOB_DISTRITO', PublicacionController.consultajsonGubernatura_distrito);
api.get('/GOB_ENTIDAD', PublicacionController.consultajsonGubernatura_entidad);
api.get('/GOB_SECCION', PublicacionController.consultajsonGubernatura_seccion);

api.get('/DIP_DET_ENTIDAD', PublicacionController.consultajsonDiputados_det_entidad);
api.get('/DIP_DISTRITO', PublicacionController.consultajsonDiputados_distrito);
api.get('/DIP_ENTIDAD', PublicacionController.consultajsonDiputados_entidad);
api.get('/DIP_SECCION', PublicacionController.consultajsonDiputados_seccion);

api.get('/MUN_DETALLE', PublicacionController.consultajsonMunicipio_detalle);
api.get('/MUN_ENTIDAD', PublicacionController.consultajsonMunicipio_entidad);
api.get('/MUN_SECCION', PublicacionController.consultajsonMunicipio_seccion);
api.get('/MUN_MUNICIPIOS', PublicacionController.consultajsonMunicipio_municipios);

api.get('/consultar', files.obtenerArchivo);

api.get('/status', files.status);

api.get('/fechaHora', PublicacionController.consultarFechaHoraActual);

module.exports = api;